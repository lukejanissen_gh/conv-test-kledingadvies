const { conversation } = require("@assistant/conversation");
const functions = require("firebase-functions");
const axios = require("axios");
const moment = require("moment-timezone");

// set locale to dutch
moment.locale("nl");

const app = conversation();
exports.ActionsOnGoogleFulfillment = functions.region("europe-west3").https.onRequest(app);

const getWeatherData = async (city) => {
  // Want to use async/await? Add the `async` keyword to your outer function/method.
  try {
    const response = await axios.get(`https://api.openweathermap.org/data/2.5/weather?q=${city}&lang=nl&units=metric&appid=5108e3869f335723ee12721719c5fb8c`);
    return response.data;
  } catch (error) {
    console.log("Error getting data: ", error);
  }
};

app.handle("initialize", async (conv) => {
  // Retrieve current time of day
  const time = moment.tz(new Date(), "Europe/Amsterdam").format("H");

  // TO DO: Generate dynamic openening message (Goedemorgen, Goedemiddag, Goedenavond) based on current time
  const opening = "Goedemorgen";

  // Save opening message to session storage
  conv.session.params.opening = opening;
});

app.handle("checkLocation", async (conv) => {
  const { currentLocation } = conv.device;

  //TO DO: get current city name from currentLocation object
  // Replace 'Amsterdam with city name from currentLocation'
  conv.session.params.city = "Amsterdam";
});

app.handle("getAdvice", async (conv) => {
  const { city } = conv.session.params;
  const weatherData = await getWeatherData(city);

  // Check if API call is successful
  if (weatherData) {
    const { temp } = weatherData.main;

    console.log("weather: ", weatherData);

    //TO DO: Set korte broek on true or false based on data from the weather API
    conv.session.params.korteBroek = "";

    // EXTRA to do: add an extra weather report (based on data from the weather API), like "Het wordt regenachtig met een temperatuur van 13 graden"
    conv.session.params.customReport = " ";
  }
});
